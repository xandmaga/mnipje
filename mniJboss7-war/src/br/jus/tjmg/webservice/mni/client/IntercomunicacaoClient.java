package br.jus.tjmg.webservice.mni.client;

import javax.xml.ws.Holder;

import br.jus.cnj.intercomunicacao_2.TipoProcessoJudicial;
import br.jus.cnj.servico_intercomunicacao_2.IntercomunicacaoService;
import br.jus.cnj.servico_intercomunicacao_2.ServicoIntercomunicacao22;
import br.jus.cnj.tipos_servico_intercomunicacao_2.TipoConsultarProcesso;
import br.jus.tjmg.webservice.mni.*;


public class IntercomunicacaoClient
{
   public String testaConsultaProcesso()
   {
        IntercomunicacaoService service = new IntercomunicacaoService();
        ServicoIntercomunicacao22 echo = service.getIntercomunicacaoImplPort();      
        TipoConsultarProcesso consultaProcesso = new TipoConsultarProcesso();
        consultaProcesso.setIdConsultante("11831846896");
        consultaProcesso.setSenhaConsultante("11831846896");
        consultaProcesso.setNumeroProcesso("6001401-32.2014.8.13.0024");
        
        Holder<String> mensagem = new Holder<String>();
        Holder<Boolean> sucesso = new Holder<Boolean>();
        Holder<TipoProcessoJudicial> processo = new Holder<TipoProcessoJudicial>();
        
        echo.consultarProcesso("11831846896", "11831846896", "6001401-32.2014.8.13.0024", "", false, false, null, sucesso, mensagem, processo);
        return (sucesso.value)? ((processo.value != null)? processo.value.getDadosBasicos().getPolo().get(1).getParte().get(0).getPessoa().getNome():""): "falhou";
   }
}