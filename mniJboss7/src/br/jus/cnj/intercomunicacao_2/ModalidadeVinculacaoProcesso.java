
package br.jus.cnj.intercomunicacao_2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de modalidadeVinculacaoProcesso.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * <p>
 * <pre>
 * &lt;simpleType name="modalidadeVinculacaoProcesso">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CX"/>
 *     &lt;enumeration value="CT"/>
 *     &lt;enumeration value="DP"/>
 *     &lt;enumeration value="AR"/>
 *     &lt;enumeration value="CD"/>
 *     &lt;enumeration value="OR"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "modalidadeVinculacaoProcesso")
@XmlEnum
public enum ModalidadeVinculacaoProcesso {

    CX,
    CT,
    DP,
    AR,
    CD,
    OR;

    public String value() {
        return name();
    }

    public static ModalidadeVinculacaoProcesso fromValue(String v) {
        return valueOf(v);
    }

}
