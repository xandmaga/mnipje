
package br.jus.cnj.intercomunicacao_2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java de tipoAssinatura complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conteúdo esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tipoAssinatura">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="assinatura" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="dataAssinatura" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="cadeiaCertificado" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="algoritmoHash" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipoAssinatura", propOrder = {
    "value"
})
public class TipoAssinatura {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "assinatura")
    protected String assinatura;
    @XmlAttribute(name = "dataAssinatura")
    protected String dataAssinatura;
    @XmlAttribute(name = "cadeiaCertificado")
    protected String cadeiaCertificado;
    @XmlAttribute(name = "algoritmoHash")
    protected String algoritmoHash;

    /**
     * Obtém o valor da propriedade value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define o valor da propriedade value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtém o valor da propriedade assinatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssinatura() {
        return assinatura;
    }

    /**
     * Define o valor da propriedade assinatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssinatura(String value) {
        this.assinatura = value;
    }

    /**
     * Obtém o valor da propriedade dataAssinatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAssinatura() {
        return dataAssinatura;
    }

    /**
     * Define o valor da propriedade dataAssinatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAssinatura(String value) {
        this.dataAssinatura = value;
    }

    /**
     * Obtém o valor da propriedade cadeiaCertificado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCadeiaCertificado() {
        return cadeiaCertificado;
    }

    /**
     * Define o valor da propriedade cadeiaCertificado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCadeiaCertificado(String value) {
        this.cadeiaCertificado = value;
    }

    /**
     * Obtém o valor da propriedade algoritmoHash.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlgoritmoHash() {
        return algoritmoHash;
    }

    /**
     * Define o valor da propriedade algoritmoHash.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlgoritmoHash(String value) {
        this.algoritmoHash = value;
    }

}
